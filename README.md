# Pokemon-Playfab
Uses [Pokemon API](https://pokeapi.co/) to fetch and catch pokemon using a simple UI on Android using Unity. Also uses [Microsoft PlayFab](https://playfab.com/) to upload and fetch scores as well as leaderboard data. 

# Images
![PokemonPlayfab](https://gitlab.com/diveshj/project-images-dump/-/raw/main/Pokemon-Playfab/Pokemon.jpg)
