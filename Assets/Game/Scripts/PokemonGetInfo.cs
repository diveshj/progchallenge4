using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PokemonGetInfo : MonoBehaviour
{
    public InputField pokemonNameTextField;

    public GameObject pImage;
    public Text pokemonNameResult;
    public Image pokemonImage;
    public GameObject catchButton;
    [HideInInspector]
    public Texture2D pSprite;
    [HideInInspector]
    public string spriteToLoad;

    [System.Serializable]
    public class Sprites
    {
        public string front_default;
    }

    [System.Serializable]
    public class Pokemon
    {
        public string name;
        public int id;
        public Sprites sprites;
    }

    private void Awake()
    {
        pokemonImage = pImage.GetComponent<Image>();
    }

    IEnumerator GetImageFromSite()
    {

        UnityWebRequest pkmimg = UnityWebRequest.Get(spriteToLoad);
        yield return pkmimg.SendWebRequest();

        if (pkmimg.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(pkmimg.error);
        }
        else
        {
            pImage.SetActive(true);
            byte[] img = pkmimg.downloadHandler.data;
            pSprite = new Texture2D(2, 2);
            pSprite.LoadImage(img);
            pokemonImage.sprite = Sprite.Create(pSprite, new Rect(0.0f, 0.0f, pSprite.width, pSprite.height), new Vector2(0.0f, 0.0f));
        }
    }

    public void GetPokemonDetails()
    {
        string pokemonName = (pokemonNameTextField.text).ToLower();
        string address = $"https://pokeapi.co/api/v2/pokemon/{pokemonName}";

        WebRequest request = WebRequest.Create(address);
        request.Method = "GET";
        request.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";

        try
        {
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseJSON = new StreamReader(responseStream).ReadToEnd();

                Pokemon pkmnResponse = JsonUtility.FromJson<Pokemon>(responseJSON);
                if (pkmnResponse != null)
                {
                    spriteToLoad = pkmnResponse.sprites.front_default;
                    StartCoroutine(GetImageFromSite());
                    pokemonNameResult.text = pkmnResponse.name;
                    catchButton.SetActive(true);
                    Debug.Log($"{pkmnResponse.name} has been seen! Its ID {pkmnResponse.id} and sprited text {pkmnResponse.sprites.front_default}");
                }
                else
                {
                    pImage.SetActive(false);
                    catchButton.SetActive(false);
                    Debug.LogError("Unable to get pokemon details");
                }
            }
        }
        catch (System.Exception ex)
        {
            pImage.SetActive(false);
            catchButton.SetActive(false);
            Debug.LogError($"Unable to get it: {ex.Message}");
        }
    }

}
