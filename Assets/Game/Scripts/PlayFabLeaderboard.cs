using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayFabLeaderboard : Singleton<PlayFabLeaderboard>
{

    //public Text HighScoreText;

    //[UnityEngine.Serialization.FormerlySerializedAs("PostLeaderboard")]
    //public Button PostLeaderboardButton;
    public Button GetLeaderboardButton;
    public Text LeaderboardTextField;

    void Start()
    {
        //PostLeaderboardButton.enabled = false;
        GetLeaderboardButton.enabled = false;
    }

    private void FixedUpdate()
    {
        if (PlayfabManager.Instance.state == PlayfabManager.LoginState.Success)
        {
            //PostLeaderboardButton.enabled = true;
            GetLeaderboardButton.enabled = true;
        }
        else
        {
            //PostLeaderboardButton.enabled = false;
            GetLeaderboardButton.enabled = false;
        }
    }

    private void OnPlayFabError(PlayFabError obj)
    {
        Debug.LogError($"PlayFab Error: {obj.ErrorMessage}");
    }

    #region Post High Score via API
    public void PostHighScore()
    {
        PlayerHighScore.high_score++;

        PlayFabClientAPI.UpdatePlayerStatistics(
            new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate>
                {
                    new StatisticUpdate { StatisticName = "high_score", Value = PlayerHighScore.high_score }
                }
            },
            OnUpdatePlayerStatistic,
            OnPlayFabError
        );
    }

    private void OnUpdatePlayerStatistic(UpdatePlayerStatisticsResult obj)
    {
        Debug.Log("Update Player Statistic Success");
    }
    #endregion


    #region Get Leaderboard via API
    public void GetLeaderboard()
    {
        PlayFabClientAPI.GetLeaderboard(
            new GetLeaderboardRequest
            {
                StatisticName = "high_score",
                StartPosition = 0,
                MaxResultsCount = 5
            },
            OnGetLeaderboardCallback,
            OnPlayFabError
        );
    }

    private void OnGetLeaderboardCallback(GetLeaderboardResult obj)
    {
        string leaderboardName = (obj.Request as GetLeaderboardRequest).StatisticName;
        Debug.Log($"Get Leaderboard success: {leaderboardName}");

        string text = "";
        foreach (PlayerLeaderboardEntry playerDetails in obj.Leaderboard)
        {
            text+= $"Player { playerDetails.DisplayName} has a rank of { playerDetails.Position} with a score of { playerDetails.StatValue} \n";
        }

        LeaderboardTextField.text = text;
    }
    #endregion

    #region Get Current Player Statistic

    [System.Serializable]
    public class PlayerHighScore
    {
        public static int high_score;
    }

    public void LoadPlayerHighScore()
    {
        PlayFabClientAPI.GetPlayerStatistics(
            new GetPlayerStatisticsRequest
            {
                StatisticNames = new List<string>() { "high_score" }
            },
            success =>
            {
                foreach(var stat in success.Statistics)
                {
                    if (stat.StatisticName == "high_score")
                    {
                        PlayerHighScore.high_score = stat.Value;
                    }
                }
            },
            OnPlayfabError
        );
    }

    public void PrintPlayerHS()
    {
        PlayFabClientAPI.GetPlayerStatistics(
    new GetPlayerStatisticsRequest
    {
        StatisticNames = new List<string>() { "high_score" }
    },
    success =>
    {
        foreach (var stat in success.Statistics)
        {
            if (stat.StatisticName == "high_score")
            {
                Debug.Log($"Player high score: {stat.Value}");
            }
        }
    },
    OnPlayfabError
);
    }


    private void OnPlayfabError(PlayFabError error)
    {
        Debug.Log("Error on stat update/get");
    }


    #endregion
}
