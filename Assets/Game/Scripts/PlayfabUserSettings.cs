using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

public class PlayfabUserSettings : MonoBehaviour
{
    public Button UpdateUsernameButton;
    public Text UsernameText;

    void Start()
    {
        UpdateUsernameButton.enabled = false;
        UsernameText.enabled = false;
    }

    private void FixedUpdate()
    {
        if (PlayfabManager.Instance.state == PlayfabManager.LoginState.Success)
        {
            UpdateUsernameButton.enabled = true;
            UsernameText.enabled = true;
        }
    }

    public void UpdateUserName()
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest()
        {
            DisplayName = UsernameText.text,
        },
        result =>
        {
            Debug.Log("Username was changed");
        },
        error =>
        {
            Debug.Log("Failed to update username");
        }
        );
    }
    public void setUsername(string _name)
    {
        UsernameText.text = _name;
    }
}
